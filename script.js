
const gameContainer = document.getElementById("game");
const mainContainer = document.querySelector(".main-container");
const startContainer = document.querySelector('.start-container');
const startButton = document.getElementById('start-button');
const selectLevel = document.querySelector('.select-level');
const easyButton = document.getElementById('easy-level');
const mediumLevel = document.getElementById('medium-level');
const hardLevel = document.getElementById('hard-level');

const restartButton = document.getElementById('restart-game');
const restartConfirm = document.querySelector('.restart-confirm');
const restartMessage = document.querySelector('.restart-message');
const confirmYesNo = document.querySelector('.confirm');
const confirmMessage = document.querySelector('.confirm');
const noMessage = document.querySelector('.no');

// const restartContainer = document.querySelector('.restart-container');



const imageUrls = [
  'gifs/1.gif',
  'gifs/2.gif',
  'gifs/3.gif',
  'gifs/4.gif',
  'gifs/5.gif',
  'gifs/6.gif',
  'gifs/7.gif',
  'gifs/8.gif',
  'gifs/9.gif',
  'gifs/10.gif',
  'gifs/11.gif',
  'gifs/12.gif',
];



function handleStart() {
  startContainer.style.display = 'none';
  selectLevel.style.display = 'flex';
  // console.log("hello")
}


startButton.addEventListener('click', handleStart);


function handleRestart() {
  restartConfirm.classList.toggle('game');
  confirmYesNo.classList.toggle('.confirm-yes');
  confirmMessage.style.display = 'block';
  // restartContainer.style.display='flex';
}

restartButton.addEventListener('click', handleRestart);

function handleNoMessage() {
  restartConfirm.classList.toggle('game');
  confirmYesNo.style.display = 'none';
}

noMessage.addEventListener('click', handleNoMessage)

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

// let shuffledColors = shuffle(IMAGES);


function handleEasyLevel(event) {

  event.target.parentElement.style.display = 'none';
  mainContainer.style.display = 'flex';

  let numberOfCards = 12;

  maxSuccess = numberOfCards / 2;

  let shuffledImages = shuffle(imageUrls).slice(0, numberOfCards / 2);
  let imagesForGame = shuffledImages.concat(shuffledImages);

  shuffle(imagesForGame);

  createDivsForColors(imagesForGame);

}


easyButton.addEventListener('click', handleEasyLevel);



function handleMediumLevel(event) {
  event.target.parentElement.style.display = 'none';
  mainContainer.style.display = 'flex';


  let numberOfCards = 16;

  maxSuccess = numberOfCards / 2;

  let shuffledImages = shuffle(imageUrls).slice(0, numberOfCards / 2);
  let imagesForGame = shuffledImages.concat(shuffledImages);

  shuffle(imagesForGame);

  createDivsForColors(imagesForGame);
}

mediumLevel.addEventListener('click', handleMediumLevel);

function handleHardLevel(event) {
  event.target.parentElement.style.display = 'none';
  mainContainer.style.display = 'flex';

  let numberOfCards = 24;

  maxSuccess = numberOfCards / 2;

  let shuffledImages = shuffle(imageUrls).slice(0, numberOfCards / 2);
  let imagesForGame = shuffledImages.concat(shuffledImages);

  shuffle(imagesForGame);

  createDivsForColors(imagesForGame);
}

hardLevel.addEventListener('click', handleHardLevel);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card



function createDivsForColors(imagesArray) {
  for (let image of imagesArray) {
    // create a new div
    const newDiv = document.createElement("div");
    const frontFace = document.createElement('img');
    const backFace = document.createElement('img');

    frontFace.classList.add('front-face');
    backFace.classList.add('back-face');

    frontFace.src = image;
    backFace.src = 'https://plus.unsplash.com/premium_photo-1678048604398-f42dda6997bd?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8N3x8cXVlc3Rpb24lMjBtYXJrfGVufDB8fDB8fHww&w=1000&q=80';

    // give it a class attribute for the value we are looping over
    // newDiv.classList.add(color);
    // call a function handleCardClick when a div is clicked on
    newDiv.classList.add(image.slice(0, 7).replace('/', '-').replace('.', ''));
    newDiv.classList.add('memory-card');
    newDiv.dataset.imageId = 0;
    newDiv.append(frontFace, backFace);

    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}





let count = 0;
let firstClick = '';
let secondClick = '';
let moves = 0;
let maxSuccess;
let success = 0;

function handleCardClick(event) {

  let movesElement = document.getElementById('moves');
  let successElement = document.querySelector('.success-container');
  let scoreCard = document.getElementById('score');
  let totalScore = document.getElementById('total-score');
  let highScore = document.getElementById('high-score');

  if (count < 2) {
    if (this.dataset.imageId == 0) {
      console.log(this, "this")
      moves++;
      this.classList.toggle('flip');
      this.dataset.imageId = 1;
      movesElement.textContent = `Moves:${moves}`;

      if (count === 0) {
        firstClick = this;
        count++;
      } else {
        secondClick = this;
        count++;
      }

      if (secondClick) {
        if (firstClick.classList[0] === secondClick.classList[0]) {
          count = 0;
          success++;


        } else {
          firstClick.dataset.imageId = 1;
          if (count === 2) {
            setTimeout(() => {
              firstClick.dataset.imageId = 0;
              secondClick.dataset.imageId = 0;
              firstClick.classList.remove('flip');
              secondClick.classList.remove('flip');
              count = 0;
              secondClick = '';
            }, 1 * 1000);
          }
        }
      }

      scoreCard.textContent = `Score: ${Math.round((success / (moves / 2)) * 100)}`;



      if (success === maxSuccess) {
        mainContainer.style.display = 'none';
        successElement.style.display = 'block';
        let finalScore = Math.round((success / (moves / 2)) * 100);
        totalScore.textContent = `Total Score: ${finalScore}`;
        let newHighScore = updateLocalStorage(finalScore);
        highScore.textContent = `High Score: ${newHighScore}`;
        highScoreGame.textContent = `High Score: ${newHighScore}`;
      }
    }
  }



  console.log("you clicked", event.target);

}


function updateLocalStorage(finalScore) {
  let newHighScore;
  if (JSON.parse(localStorage.getItem('highScore')) === null) {
    localStorage.setItem('highScore', finalScore);
    newHighScore = finalScore;
  } else {
    newHighScore = JSON.parse(localStorage.getItem('highScore'));
    if (newHighScore < finalScore) {
      localStorage.setItem('highScore', JSON.stringify(finalScore));
      newHighScore = finalScore
    }
  }
  return newHighScore;
}







